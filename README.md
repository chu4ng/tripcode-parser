Usage:
```
./trip --help
./trip -m "COVID" -c 1 -i -s 24554
```
Output example:
```
Pass: 7]<qDCb   Trip: !pWSCOvIDLA
```
If defined DEV as 1, a program print seed and count of iteration:
```
Seed is 1591235563
Pass: -[X+ASy   Trip: !x.cOVId24k
Iteration: 11263511
```
Help:
```
Usage: trip [OPTION...] -m PATTERN -c COUNT
Maining tripcodes by pattern, count and case sensitivity

  -c, --count=NUMBER         Number of getting tripcodes.
  -i, --ignore_case          Ignor case for matched tripcodes
  -m, --match=PATTERN        Pattern for match tripcodes.
  -s, --seed=SEED            Seed for generate tripcodes.
  -?, --help                 Give this help list
      --usage                Give a short usage message
  -V, --version              Print program version
```
Compile:
```
make
```
For compile with seed and iteration information:
```
make "CFLAGS=-DDEV=1"
```
For check tripcode with other methods: http://desktopthread.com/tripcode.php
