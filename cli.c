#include <stdio.h>
#include <stdlib.h>
#include <argp.h>
#include <stdbool.h>
#include "trip.h"

#if DEV != 2
const char *argp_program_version = "Version of tripcode: 0.3";
const char *argp_program_bug_address = "<shichuang@horsefucker.org>";
static char doc[] = "Maining tripcodes by pattern, count and case sensitivity";
static char args_doc[] = "-m PATTERN -c COUNT";
static error_t parse_opt(int key, char *arg, struct argp_state *state);
static struct argp_option options[] = { 
	{ "match", 'm', "PATTERN", OPTION_NO_USAGE, "Pattern for match tripcodes."},
	{ "count", 'c', "NUMBER", OPTION_NO_USAGE, "Number of getting tripcodes."},
	{ "seed", 's', "SEED", 0, "Seed for generate tripcodes."},
	{ "ignore_case",'i', 0, OPTION_ARG_OPTIONAL, "Ignor case for matched tripcodes"},
	{ 0 } 
};

static struct argp argp= { options, parse_opt, args_doc, doc, 0, 0, 0 };

struct arguments {
	char *match_str;
	uint8_t count;
	uint32_t seed;
	bool ignoreCase;
	uint8_t req;
};

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;
	switch (key) {
		case 'm': arguments->match_str = arg; arguments->req += 1; break;
		case 'c': arguments->count = atoi(arg);  arguments->req += 1; break;
		case 'i': arguments->ignoreCase = true; break;
		case 's': arguments->seed = atoi(arg); break;
		case ARGP_KEY_ARG: return 0;
		case ARGP_KEY_END:
			if (arguments->req < 2) {
			   argp_usage(state);
			   printf("Too few arguments!\n");
		    }
		default: return ARGP_ERR_UNKNOWN;
	}   
	return 0;
}

#endif

int main(int argc, char *argv[]) {
	char *match;
	uint16_t count;
	bool ignor;
	uint32_t seed;

#if DEV == 2
	match = "cov"; count = 5; ignor = 1; seed = 1591235563;
#else
	struct arguments arguments;

    arguments.ignoreCase = false;
    arguments.req = 0;
    arguments.seed = time(0);

    argp_parse(&argp, argc, argv, 0, 0, &arguments);	

	match = arguments.match_str;
	count = arguments.count;
	ignor = arguments.ignoreCase;
	seed = arguments.seed;
#endif

	srand(seed);
#if DEV == 1
	printf("Seed is %d\n", seed);
#endif
	
	uint8_t len = 8;
	char buf[10], seq[len];
	uint32_t iter = 0;
	while(count) {
		iter++;
		gener_seq(seq, len);
		encode_tripcode(seq, buf);
		if(compare_trips(buf, match, ignor)) {
			printf("Pass: %s\tTrip: !%s\n", seq, buf);
			count--;
		}
	}
#if DEV == 1
	printf("Iteration: %d\n", iter);
#endif
}

