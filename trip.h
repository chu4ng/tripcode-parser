#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <time.h> 
#include <stdint.h>
#include <stdbool.h>

void encode_tripcode(char *pattern, char *buf_str); // Make tripcode from char seq.
bool compare_trips(const char *current_code, const char *match, bool ignore_flag);
// Generate random sequence
void gener_seq(char *buf_str, uint8_t len);
