CC = gcc
LIBS = -lcrypt
OUTNAME = trip
OBJ = trip.o cli.o
CFLAGS = -DDEV=0 -Wall -Wpedantic

all: $(OBJ)
	$(CC) $(LIBS) $? -o $(OUTNAME)
ctags:
	gcc -M *.c *.h | sed -e 's/[\\ ]/\n/g' | sed -e '/^$$/d' -e '/\.o:$$/d' | ctags -L - --c++-kinds=+p --fields=+iaS --extras=+q

%.o: %.c
	$(CC) $(CFLAGS) -c $? -O3 -o $@
clean:
	rm *.o
cleana:
	rm *.o $(OUTNAME)
