#include "trip.h"

static char alph[] = {'!', '"', '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '-', '.', '/', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', '\\', ']', '^', '_', '`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '{', '|', '}'};
static uint8_t alph_l = sizeof(alph);
// Alph for replacement *bad* characters
static char alph_s1[] = ":;<=>?@[\\]^_`-,|+(){}\"'%&$!#*";
static char alph_s2[] = "ABCDEFGabcdef................";

void encode_tripcode(char *pattern, char *buffer) {
	char buf_salt[3];
	strncpy(buf_salt, pattern+1, 2);

	for (uint8_t i = 0; i < 2; i++) {
		for (uint8_t j = 0; j < strlen(alph_s1); j++)
			if(buf_salt[i] == alph_s1[j]) {
				buf_salt[i] = alph_s2[j];
			}
	}
	buf_salt[2] = '\0';

	char *trip_buffer = crypt(pattern, buf_salt);
	strncpy(buffer, trip_buffer+(strlen(trip_buffer)-10), 10);
}
bool compare_trips(const char *trip, const char *match, bool ignore) {
	char *p;
	if(!ignore)
		p = (char *)strstr(trip, match);
	else 
		p = strcasestr(trip, match);
	return (p) ? 1 : 0;
}
void gener_seq(char *buffer, uint8_t len) {
	for(uint8_t i = 0; i < len; i++)
		buffer[i] = alph[rand() % alph_l];
	buffer[len-1] = '\0';
}
